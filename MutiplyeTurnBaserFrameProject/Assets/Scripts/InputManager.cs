﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridGame.Input.Manager
{
    public class InputManager : MonoBehaviour
    {
        private Action OnButtonPressed;
        public bool ignoreInput;
        public void Init(Action OnButtonPress)
        {
            this.OnButtonPressed = OnButtonPress;
        }
        
        /// <summary>
        /// This function is called when player give input on grid
        /// </summary>
        public void OnClickOnGrid()
        {
            if(ignoreInput)
                return;
            OnButtonPressed?.Invoke();
            Handheld.Vibrate();
        }
    }
}
