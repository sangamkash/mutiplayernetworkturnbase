﻿using System;
using System.Collections;
using System.Collections.Generic;
using GridGame.Contants;
using GridGame.Core.Network;
using GridGame.Input.Manager;
using GridGame.Network.Manager;
using GridGame.Player.Manager;
using GridGame.UI;
using UnityEngine;
using  UnityEngine.SceneManagement;
namespace GridGame.Game.Manager
{
    public class GameManager : MonoBehaviour,INetworkCallBack
    {
        public static GameManager Instance;
        public RoomInfo currentRoomInfo;
        [Header("External Dependency")] 
        [SerializeField] private NetworkManager networkManager;
        private AsyncOperation asyncLoading;
        private PlayerManager playerManager;
        private InputManager inputManager;
        private GameOverScreen gameOverScreen;
        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        #region UI_Functions

        public void OnNameChange(string name)
        {
            networkManager.roomInfo.myData.playerData.playerName = name;
        }
        public void StartGame()
        {
            networkManager.ConnnectToServer(ServerLocation.None, this);
        }
        
        #endregion
        
        #region NetworkCallBacks

        public void OnConnectedToNetwork()
        {
            networkManager.CreateRoomOrJoin();
        }

        public void OnFailedToConnectNetwork(NetworkError error, string msg)
        {
            
        }

        public void OnDisconnectedToNetwork()
        {
            
        }

        public void OnRoomCreatedOrJoined(RoomInfo roomInfo)
        {
            currentRoomInfo = roomInfo;
#if CUSTOM_LOGS
         Debug.Log($"<color=green> successfully joined a room name: <color=yellow>{roomInfo.roomName}</color> with my roomId : <color=yellow>{roomInfo.myData.roomId}</color> </color>");
#endif
            LoadGamePlay();
        }

        public void OnRoomJoined(RoomInfo roomInfo)
        {
            currentRoomInfo = roomInfo;
        }

        public void OnFailedToJoinRoom(NetworkError error, string msg)
        {
            
        }

        public void OnPlayerJoin(PlayarDataRoom playerData)
        {
            
        }

        public void OnPlayerRemoved(PlayarDataRoom playerData)
        {
            
        }

        public void OnPlayerTurnChange(PlayarDataRoom playerData,float timeRemaining)
        {
            inputManager.ignoreInput = playerData.playerData.id != currentRoomInfo.myData.playerData.id;
            playerManager.UpdatePlayersTurn(playerData.playerData, timeRemaining);
        }

        public void OnStartGameInTime(float time)
        {
            FindObjectOfType<LoadingScreen>().Init(time);
            playerManager = FindObjectOfType<PlayerManager>();
        }

        public void OnFailToStartTheGame(NetworkError networkError)
        {
            
        }

        public void OnGameSessionEnd()
        {
            gameOverScreen.gameObject.SetActive(true);
        }

        #endregion

        #region Misc

        private void LoadGamePlay()
        {
              asyncLoading=SceneManager.LoadSceneAsync(GameContants.GAMEPLAY_SCENE, LoadSceneMode.Single);
              StartCoroutine(CheckLoadingCompleted(asyncLoading));
        }

        private IEnumerator CheckLoadingCompleted(AsyncOperation asyncOperation)
        {
            yield return new WaitUntil(() => asyncOperation.isDone);
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            OnSceneLoadingDone();
        }

        private void OnSceneLoadingDone()
        {
            inputManager = FindObjectOfType<InputManager>();
            gameOverScreen = FindObjectOfType<GameOverScreen>();
            gameOverScreen.Init(Restart);
            inputManager.Init(networkManager.PerformInputAction);
            networkManager.StartGame();
            gameOverScreen.gameObject.SetActive(false);
        }

        public void Restart()
        {
            Destroy(networkManager.gameObject);
            Destroy(gameObject);
        }
        #endregion
    }
}
