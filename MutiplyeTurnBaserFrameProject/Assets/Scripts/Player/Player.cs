﻿using System.Collections;
using System.Collections.Generic;
using GridGame.Data;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace GridGame.Player
{
    public class Player : MonoBehaviour, IPlayer
    {
        [Header("Ref")]
        [SerializeField] private TextMeshProUGUI playerNameTxt;
        [SerializeField] private TextMeshProUGUI scoreTxt;
        [SerializeField] private GameObject selfHighLightGameObj;
        [SerializeField] private Image profilePic;
        [SerializeField] private Image timerCellImg;
        [SerializeField] private Image fillTimerImg;

        public PlayerData PlayerData{ get; private set;}
        public void Init(PlayerData playerData, bool self)
        {
            PlayerData = playerData;
            playerNameTxt.text = playerData.playerName;
            selfHighLightGameObj.SetActive(self);
            scoreTxt.text = "0";
        }

        public void HighLightwithColor(Color color)
        {
            profilePic.color = color;
            timerCellImg.color = color;
        }

        public void UpdateTimer(float fillAmount)
        {
            fillTimerImg.fillAmount = fillAmount;
        }
    }
}
