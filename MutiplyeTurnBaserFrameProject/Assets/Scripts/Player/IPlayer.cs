﻿using System.Collections;
using System.Collections.Generic;
using GridGame.Data;
using UnityEngine;

public interface IPlayer
{
    void Init(PlayerData playerData, bool self);
    /// <summary>
    /// This function will change the color of player images
    /// </summary>
    /// <param name="color">color recommended is gery for un-highlight and while for highlight </param>
    void HighLightwithColor(Color color);

    /// <summary>
    /// 
    /// </summary>
    /// <param name="fillAmount">The value range form [0f,1f]</param>
    void UpdateTimer(float fillAmount);

}
