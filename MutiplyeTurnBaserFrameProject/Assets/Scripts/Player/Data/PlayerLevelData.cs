﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridGame.Data
{
    /// <summary>
    /// We have created it as ScriptableObject so that we can configure the data in editor
    /// TODO : This need to be change to Serializable class instead of ScriptableObject as has been done temporarily so that we can configure it in editor as simulation data
    /// This will be store in the server side 
    /// </summary>
    [CreateAssetMenu(fileName = "LevelData", menuName = "GridGame/PlayerLevelData", order = 2)]
    public class PlayerLevelData : ScriptableObject
    {
        public int defenceLevel;
        public int attackLevel;
        public int troopStrength;
    }
}