﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace GridGame.Data
{
    /// <summary>
    /// We have created it as ScriptableObject so that w can configure the data in editor
    /// TODO : this need to be change to Serializable class instead of ScriptableObject as has been done temporarily so that we can configure it in editor as simulation data
    /// This will be store in the server side 
    /// </summary>
    [CreateAssetMenu(fileName= "PlayerData",menuName= "GridGame/PlayerData",order = 1)]
    public class PlayerData : ScriptableObject
    {
        [FormerlySerializedAs("name")] 
        public string playerName;
        public string id;
        public uint score;
        public PlayerLevelData levelData;
    }
}
