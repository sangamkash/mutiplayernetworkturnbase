﻿using System;
using System.Collections;
using System.Collections.Generic;
using GridGame.Data;
using GridGame.Game.Manager;
using UnityEngine;

namespace GridGame.Player.Manager
{
    public class PlayerManager : MonoBehaviour
    {
        private GameManager pGameManager=>GameManager.Instance;
        [Header("Configuration")]
        [SerializeField] private Color hightLightColor;
        [SerializeField] private Color unHightLightColor;
        [SerializeField] private Player[] allPlayer;

        private void Start()
        {
            Init();
        }

        private void Init()
        {
            for (int i = 0; i < allPlayer.Length; i++)
            {
                var data = pGameManager.currentRoomInfo.players[i].playerData;
                allPlayer[i].Init(data, pGameManager.currentRoomInfo.myData.playerData.id == data.id);
                allPlayer[i].HighLightwithColor(unHightLightColor);
                allPlayer[i].UpdateTimer(0);
            }
        }

        public void UpdatePlayersTurn(PlayerData currentPlayerTurn, float time)
        {
            StopAllCoroutines();
            for (int i = 0; i < allPlayer.Length; i++)
            {
                var isCurrentplayer = allPlayer[i].PlayerData.id == currentPlayerTurn.id;
                var color = isCurrentplayer ? hightLightColor : unHightLightColor;
                var timeBarValue = isCurrentplayer ? 1f : 0f;
                allPlayer[i].HighLightwithColor(color);
                allPlayer[i].UpdateTimer(timeBarValue);
                if (isCurrentplayer)
                    StartCoroutine(UpdateUIPlayerUI(allPlayer[i], time, time));
            }
        }

        private IEnumerator UpdateUIPlayerUI(Player player,float timeRemaining,float originalTime)
        {
            if (timeRemaining <= 0)
            {
                player.UpdateTimer(0f);
            }
            else
            {
                player.UpdateTimer(timeRemaining/originalTime);
                timeRemaining -= Time.deltaTime;
                yield return new WaitForEndOfFrame();
                StartCoroutine(UpdateUIPlayerUI(player, timeRemaining, originalTime));
            }
        }
        
    }
}
