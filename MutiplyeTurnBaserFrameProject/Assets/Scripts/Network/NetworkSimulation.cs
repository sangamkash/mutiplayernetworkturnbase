﻿using System.Collections;
using System.Collections.Generic;
using GridGame.Core.Network;
using UnityEditor.Experimental;
using UnityEngine;
using UnityEngine.Serialization;

namespace GridGame.Core.Network.Simulation
{
    public class NetworkSimulation : BaseNetwork
    {
         [Header("SimulationData")]
         public RoomInfo roomInfo;
         [SerializeField] private float startTime = 5f;
         [SerializeField] private float turnSessionTime = 6f;
         [SerializeField] private int totalTurnCount = 5;
         
         //Temp Simuluation var 
         private int counter;
         private int playerIndex = 0;
        public override void ConnnectToServer(ServerLocation serverLocation, INetworkCallBack networkCallBack)
        {
            base.ConnnectToServer(serverLocation, networkCallBack);
            OnConnectedToNetwork();
            NetworkCallBack?.OnConnectedToNetwork();
        }

        public override void CreateRoomOrJoin(RoomParameter roomParameter=null)
        {
            base.CreateRoomOrJoin(roomParameter);
            OnRoomCreatedOrJoined(roomInfo);
            NetworkCallBack?.OnRoomCreatedOrJoined(roomInfo);
        }

        public override void StartGame()
        {
            base.StartGame();
            OnStartGameInTime(startTime);
            NetworkCallBack?.OnStartGameInTime(startTime);
        }

        public override void OnStartGameInTime(float startTime)
        {
            StartCoroutine(StartTurns(startTime));
        }
        
        #region SimuationFunctions

        /// <summary>
        /// This functionality need to be implemented to the server side 
        /// </summary>
        /// <param name="startTime"></param>
        /// <returns></returns>
        IEnumerator StartTurns(float startTime)
        {
            yield return new WaitForSeconds(startTime);
            var totalnumberOfPlayer = 4;
            counter = totalTurnCount * 4;
            playerIndex = 0;
            StartCoroutine(RepeatTheTurn());
        }

        IEnumerator RepeatTheTurn()
        {
            while (counter>0)
            {
                var radomInputSimulation = 0f;
                if (playerIndex != 0)
                    radomInputSimulation = Mathf.Clamp(Random.Range(-3.5f, 2f), -3.5f, 0f);
                if (playerIndex >= 4)
                    playerIndex = 0;
                NetworkCallBack.OnPlayerTurnChange(roomInfo.players[playerIndex],turnSessionTime);
                counter--;
                playerIndex++;
                yield return new WaitForSeconds(turnSessionTime+radomInputSimulation);
            }

            NetworkCallBack.OnGameSessionEnd();
        }

        public void PerformInputAction()
        {
            StopAllCoroutines();
            StartCoroutine(RepeatTheTurn());
        }

        #endregion
        
    }
}
