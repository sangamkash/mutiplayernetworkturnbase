﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using GridGame.Data;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

namespace GridGame.Core.Network
{
    #region NetworkClasses

    /// <summary>
    /// This enum represent the server locations
    /// </summary>
    [System.Serializable]
    public enum ServerLocation
    {
        None=0,
        America=01,
        Africa=02,
        SouthAsia=03,
        NorthAsia=04,
        Europe=05
    }
   
    /// <summary>
    /// This enum represent all network errors
    /// </summary>
    [System.Serializable]
    public enum NetworkError
    {
        //General error
        
        NoInternet=102,
        BadRequest=400,
        NotFound=404,
        TimeOut=405,
        //Room Related Error
        FirstConnectToServer=600,
        RoomDoesntExists=601,
        //Misc
        NullParameter=1001,
        Unknown=1010
        
    }

    /// <summary>
    /// This class is used for passing info to server for creating a room with custom parameter
    /// </summary>
    [System.Serializable]
    public class RoomParameter
    {
        public bool joinToRandomRoom;
        public string roomName;
        public int numberOfPlayer;
        public PlayerLevelData matchMakingLevelData;

        /// <summary>
        /// simple join a random Room;
        /// </summary>
        public RoomParameter()
        {
            joinToRandomRoom = true;//Once bool is true server will provide rest of the parameter such as RoomName ,matchMakingLevelData
            roomName = "RandomRoom";
            numberOfPlayer = 2;
            matchMakingLevelData = null;
        }

        /// <summary>
        /// This will create a custom room with default matchmaking data
        /// </summary>
        /// <param name="roomName">Room name must have at least 3 character </param>
        /// <param name="numberOfPlayer">number of player should be more then 1 </param>
        public RoomParameter(string roomName, int numberOfPlayer)
        {
            this.roomName = roomName;
            this.numberOfPlayer = numberOfPlayer;
        }
        
        /// <summary>
        /// This will create a custom room with default matchmaking data
        /// </summary>
        /// <param name="roomName">Room name must have at least 3 character </param>
        /// <param name="numberOfPlayer">number of player should be more then 1 </param>
        /// <param name="matchMakingLevelData">should not be null </param>
        public RoomParameter(string roomName, int numberOfPlayer,PlayerLevelData matchMakingLevelData)
        {
            this.roomName = roomName;
            this.numberOfPlayer = numberOfPlayer;
            this.matchMakingLevelData = matchMakingLevelData;
        }
    }

    [System.Serializable]
    public class PlayarDataRoom
    {
        public uint roomId;
        public PlayerData playerData;
    }
    /// <summary>
    /// This represent the room Info 
    /// </summary>
    [System.Serializable]
    public class RoomInfo
    {
        public string roomName;
        public PlayarDataRoom myData;
        public int numberOfPlayer;
        public PlayarDataRoom[] players;
    }
    
    #endregion
    
    public abstract class BaseNetwork : MonoBehaviour,INetworkCallBack
    {
        public INetworkCallBack NetworkCallBack { get; private set; }
        
        #region  StateOfNwtwork
        public static string playerId { get; private set; }
        public static uint playerRoomId { get; private set; }
        public static bool ConnectedToServer { get; private set; }
        public static bool ConnectedToRoom { get; private set; }
        
        public ServerLocation ConnectedServer { get; private set; }
        
        #endregion
        /// <summary>
        /// This will initial connecting to given server
        /// </summary>
        /// <param name="serverLocation">This define the network location Note: "None" will auto pic best network for client</param>
        public virtual void ConnnectToServer(ServerLocation serverLocation,INetworkCallBack networkCallBack)
        {
#if NETWORK_LOGS
            Debug.Log($"<color=green>Initialed connection to server {serverLocation.ToString()}</color>");
#endif
            this.NetworkCallBack = networkCallBack;
            if (Application.internetReachability == NetworkReachability.NotReachable)
            {
#if NETWORK_LOGS
                Debug.Log("Check the interet Connection");
#endif
                OnFailedToConnectNetwork(NetworkError.NoInternet, "Check the internet Connection");
                this.NetworkCallBack?.OnFailedToConnectNetwork(NetworkError.NoInternet,"Check the internet Connection");
                return;
            }
            //TODO : Write a logic to connect the network
        }

        public virtual void CreateRoomOrJoin(RoomParameter roomParameter = null)
        {
#if NETWORK_LOGS
            Debug.Log($"<color=green>Initialed room  Creation {roomParameter?.roomName ?? "Random-room"}</color>");
#endif
            if (!ConnectedToServer)
            {
#if NETWORK_LOGS
                Debug.Log("First connect to Server");
#endif
                OnFailedToConnectNetwork(NetworkError.FirstConnectToServer,"First connect to Server");
                NetworkCallBack?.OnFailedToConnectNetwork(NetworkError.FirstConnectToServer,"First connect to Server");
                return;
            }
            //TODO : Write a logic to create a room
        }

        public virtual void StartGame()
        {
            //TODO : Write a logic to start the game 
        }
        
        #region NetworkCallBack
        
        public virtual void OnConnectedToNetwork()
        {
            ConnectedToServer = true;
        }

        public virtual void OnFailedToConnectNetwork(NetworkError error, string msg)
        {
            ConnectedToServer = false;
        }

        public virtual void OnDisconnectedToNetwork()
        {
            ConnectedToServer = false;
        }

        public virtual void OnRoomCreatedOrJoined(RoomInfo roomInfo)
        {
            ConnectedToRoom = true;
            playerId = roomInfo.myData.playerData.id;
            playerRoomId = roomInfo.myData.roomId;
        }

        public virtual void OnRoomJoined(RoomInfo roomInfo)
        {
            ConnectedToRoom = true;
            playerId = roomInfo.myData.playerData.id;
            playerRoomId = roomInfo.myData.roomId;
        }

        public virtual void OnFailedToJoinRoom(NetworkError error, string msg)
        {
            ConnectedToRoom = false;
            playerRoomId = 0;
        }

        public virtual void OnPlayerJoin(PlayarDataRoom playerData)
        {
            
        }

        public virtual void OnPlayerRemoved(PlayarDataRoom playerData)
        {
            
        }

        public virtual void OnPlayerTurnChange(PlayarDataRoom playerData, float timeRemaining)
        {
            
        }

        public virtual void OnStartGameInTime(float time)
        {
            
        }

        public virtual void OnFailToStartTheGame(NetworkError networkError)
        {
            
        }

        public void OnGameSessionEnd()
        {
            
        }

        #endregion
    }
}
