﻿using System.Collections;
using System.Collections.Generic;
using GridGame.Data;
using UnityEngine;

namespace GridGame.Core.Network
{

    /// <summary>
    /// Note: All  the function call are make via TCP protocol so there is no loss of information  
    /// </summary>
    public interface INetworkCallBack
    {
        /// <summary>
        /// This function receive callback when a device is connected to is server 
        /// </summary>
        void OnConnectedToNetwork();

        /// <summary>
        /// This function receive callback when a device is failed to connected to is server 
        /// </summary>
        /// <param name="error">this provide an info about the cause of disconnect</param>
        void OnFailedToConnectNetwork(NetworkError error,string msg);
        
        /// <summary>
        /// This function receive callback when network is disconnected
        /// </summary>
        void OnDisconnectedToNetwork();

        /// <summary>
        ///  This function receive callback when the client who is connected to the network has created and joined room successfully 
        /// </summary>
        /// <param name="roomInfo">This give all the details of room</param>
        void OnRoomCreatedOrJoined(RoomInfo roomInfo);
        
        /// <summary>
        /// This function receive callback when the client who is connected to the network joined room successfully 
        /// </summary>
        ///  <param name="roomInfo">This give all the details of room</param>
        void OnRoomJoined(RoomInfo roomInfo);
        
        /// <summary>
        /// This function receive callback when the client who is connected to the network and joined room successfully 
        /// </summary>
        /// <param name="error"></param>
        void OnFailedToJoinRoom(NetworkError error, string msg);
        
        /// <summary>
        /// This function receive callback for all the client who are connected to the network and joined room successfully 
        /// When every a new player is joined
        /// </summary>
        /// <param name="playerData"></param>
        void OnPlayerJoin(PlayarDataRoom playerData);

        /// <summary>
        /// This function receive callback for all the client who are connected to the network and joined room successfully 
        /// When every a any player is removed
        /// </summary>
        /// <param name="palyerData"></param>
        void OnPlayerRemoved(PlayarDataRoom playerData);

        /// <summary>
        /// This receive callback for all the client in the room who are connected to the network and joined room successfully
        /// When every the turn is shifted from one player to other player
        /// </summary>
        /// <param name="playerData"></param>
        /// <param name="timeRemaining"> This show how much time is remaining for switch the turn</param>
        void OnPlayerTurnChange(PlayarDataRoom playerData,float timeRemaining);

        /// <summary>
        /// This receive callback for all the client in the room who are connected to the network and joined room successfully
        /// When the game is about to start
        /// </summary>
        /// <param name="time">this represent time to start the game </param>
        void OnStartGameInTime(float time);
        
        /// <summary>
        /// This receive callback for all the client in the room who are connected to the network and joined room successfully
        /// When the game is failed to start
        /// </summary>
        /// <param name="networkError"></param>
        void OnFailToStartTheGame(NetworkError networkError);

        /// <summary>
        /// This receive callback for all the client in the room who are connected to the network and joined room successfully
        /// When the game is failed to start
        /// </summary>
        void OnGameSessionEnd();
    }
}
