﻿using System;
using System.Collections;
using System.Collections.Generic;
using GridGame.Core.Network;
using GridGame.Core.Network.Simulation;
using GridGame.Data;
using UnityEngine;

namespace GridGame.Network.Manager
{
    public class NetworkManager : NetworkSimulation
    {
        private void Awake()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
