﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GridGame.Contants
{
    public static class GameContants
    {
        public const string GAMEPLAY_SCENE = "GamePlay";
        public const string LOADING_SCENE = "Loading";

    }
}
