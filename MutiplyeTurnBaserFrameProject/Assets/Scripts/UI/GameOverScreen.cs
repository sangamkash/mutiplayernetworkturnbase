﻿using System;
using System.Collections;
using System.Collections.Generic;
using GridGame.Contants;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GridGame.UI
{
    public class GameOverScreen : MonoBehaviour
    {
        private Action action;
        public void Init(Action action)
        {
            this.action = action;
        }
        public void RestartGame()
        {
            action?.Invoke();
            SceneManager.LoadScene(GameContants.LOADING_SCENE);
        }
    }
}
