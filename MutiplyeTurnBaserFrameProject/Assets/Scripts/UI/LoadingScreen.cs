﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using TMPro;
using Unity.Collections;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GridGame.UI
{
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] private GameObject syncTimeGameObj;
        [SerializeField] private TextMeshProUGUI timeRemainingText;
        [SerializeField] private Image fillImag;

        public void Init(float time)
        {
            syncTimeGameObj.SetActive(false);
            StartCoroutine(StartTimer(time));
        }
        
        private void LoadingTimer(float timer)
        {
            timeRemainingText.text = ((int) timer).ToString();
            fillImag.fillAmount = timer % 1f;
        }

        /// <summary>
        /// Note This is a temp solution for check need to replace with real time as pausing the application or movaing an application background effect it
        /// </summary>
        /// <param name="timeRemaining"></param>
        /// <returns></returns>
        private IEnumerator StartTimer(float timeRemaining)
        {
            if(timeRemaining<=0)
                gameObject.SetActive(false);
            LoadingTimer(timeRemaining);
            yield return new WaitForEndOfFrame();
            StartCoroutine(StartTimer(timeRemaining - Time.deltaTime));
        }
    }
}
